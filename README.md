# Le fichier hosts
Le fichier hosts peut-être utilisé pour bloquer l'acces à certains domaines depuis votre ordinateur ou votre réseau.

Pour plus d'informations concernant les fichiers hosts vous pouvez consulter [cette page wikipedia](https://fr.wikipedia.org/wiki/Hosts).



---


### Où trouver le fichier Hosts ?

* Sous Linux:     **/etc/hosts**
* Sous MacOS:     **/etc/hosts**
* Sous Windows:   **C:\Windows\System32\drivers\etc\hosts**




---


### Fichiers hosts de ce dépot:

#### spotify  https://framagit.org/troll/hosts/raw/master/spotify
Il suffit d'ajouter les lignes contenues dans le fichier ci-dessus dans votre fichier hosts pour bloquer les publicités Spotify.